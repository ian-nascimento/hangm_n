int gerar_numero_aleatorio(int valor_inicial, int valor_final){
  srand(time(NULL));
  int numero_aleatorio = valor_inicial + (rand() % valor_final);
  return numero_aleatorio;
}

int obter_numero_de_linhas_do_arquivo(FILE *ponteiro_auxiliar){
  char palavra_auxiliar[TAMANHO_DA_PALAVRA];
  int numero_de_linhas_do_arquivo = 0;

  while((fscanf(ponteiro_auxiliar,"%s\n", palavra_auxiliar))!=EOF){
    numero_de_linhas_do_arquivo += 1;
  }
  // Faz o ponteiro apontar para a posicao inicial do arquivo
  rewind(ponteiro_auxiliar);

  return numero_de_linhas_do_arquivo;
}
