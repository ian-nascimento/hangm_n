#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <ctype.h>
#include <unistd.h>
#include "forca_vars.h"
#include "funcoes_auxiliares.c"
#include "forca_validacoes.c"
#include "forca_output.c"
#include "forca_input.c"


int main(){
  definir_palavra();
  do{
    imprimir_forca();
    informar_letra();
    preencher_forca();
  }while(!perdeu() && !ganhou());

  imprimir_forca();

  return 0;
}
