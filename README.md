## Hangm_n

Um simples jogo da forca (hangman) feito em C para fins de estudo.


### Como jogar

Após fazer o clone deste repositório, execute os seguintes comandos a partir dele:

```
g++ forca.c -o forca.out
./forca.out
```

Você pode incluir palavras no arquivo [palavras.txt](https://gitlab.com/iancsn/hangm_an/-/blob/master/palavras.txt) com o seguinte comando:

```
echo "algumapalavra" >> palavras.txt
```

### Capturas de tela
![](https://gitlab.com/iancsn/hangm_an/-/raw/master/imagens/hangmandemo.png)

### A fazer

- [x] O programa deve ler um conjunto de palavras de um arquivo txt e selecionar aleatoriamente uma das palavras da lista.
- [x] O programa deve mostrar um "\_" para cada caracter e, ao acertar, deve-se exibi-lo ao invés do "\_", assim como a lista dos caracteres já usados e os erros cometidos.
- [x] O jogador pode errar apenas 4  vezes e, a cada erro, a forca deve ser desenhada no terminal.
- [ ] Adicionar função para que o jogador possa especificar o arquivo com as palavras que deverão ser sorteadas.
- [ ] Adicionar função para que o jogador possa definir a dificuldade do jogo (cada dificuldade correspondendo ao tamanho da palavra e o total de erros possíveis).
- [ ] Adicionar função que permita ao jogador obter palavras aleatórias pela internet.
- [ ] Configurar a impressão da forca para que o "\_" não seja exibido para caracteres especiais como o espaço
