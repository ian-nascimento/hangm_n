int ganhou(){
  if(total_de_acertos == strlen(palavra_definida)){
    return 1;
  }
  return 0;
}

int perdeu(){
  if(total_de_erros == 4){
    return 1;
  }
  return 0;
}

int tentou(){
  for(int indice=0; indice<TAMANHO_DA_PALAVRA; indice++){
    if(letra_informada_pelo_jogador == tentativas[indice]){
      return 1;
    }
  }
  return 0;
}
